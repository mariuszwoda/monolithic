/**
 * Data Access Objects used by WebSocket services.
 */
package com.w2p.fi.web.websocket.dto;
