/**
 * View Models used by Spring MVC REST controllers.
 */
package com.w2p.fi.web.rest.vm;
